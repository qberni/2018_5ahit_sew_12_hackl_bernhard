﻿using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Person;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_StudentsTest
{
    [TestClass]
    public class ExtensionTest
    {
        [TestMethod]
        public void FindStudentsBetweenAgeTest()
        {
            IEnumerable<Student> students = Context.Instance.StudentRepo.FindStudentsBetweenAge(1, 18);
            Assert.AreEqual(3, students.Count());
        }

        [TestMethod]
        public void FindStudentsWithAgeTest()
        {
            IEnumerable<Student> students = Context.Instance.StudentRepo.FindStudentsWithAge(18);
            Assert.AreEqual(3, students.Count());
        }

        [TestMethod]
        public void FinStudentsFirstAndLAstTest()
        {
            IEnumerable<Student> std = Context.Instance.StudentRepo.FindStudentByFirstAndLAst("Dominik", "Dafert");
            Assert.AreEqual(1,std.Count());
        }

        [TestMethod]
        public void FindStudentsOrderbyAgeTest()
        {
            IEnumerable<Student> students = Context.Instance.StudentRepo.FindStudentsOrderByAge();
            Student[] std = students.Cast<Student>().ToArray();
            Assert.AreEqual(18, std[1].Age);
        }

        [TestMethod]
        public void FindStudentsByLastNameContaining()
        {
            IEnumerable<Student> students = Context.Instance.StudentRepo.FindStudentsWithLastNameContains("auer");
            Assert.AreEqual(2, students.Count());
        }

        [TestMethod]
        public void FindstudentsMaxAge()
        {
            IEnumerable<Student> std = Context.Instance.StudentRepo.FindStudentsOrderByMaxAge();
            Student[] studnet = std.Cast<Student>().ToArray();
            Assert.AreEqual(22, studnet[0].Age);
        }

        /*
        [TestMethod]
        public void FindStudentsBySchoolTest()
        {
            Context c = Context.Instance;
            School s = c.SchoolRepo.Find(1);
            IEnumerable<Student> students = c.StudentRepo.FindStudentsBySchool(s);
            Assert.AreEqual(0, students.Count());

        }
        */
        /*
        [TestMethod]
        public void FindTeachersBySubjectTest()
        {
            Context c = Context.Instance;
            Subject s = c.SubjectRepo.Find(1);
            IEnumerable<Teacher> teachers = c.TeacherRepo.FindTeachersBySubject(s);
            Assert.AreEqual(2, teachers.Count());

        }
        */
        [TestMethod]
        public void FindTeachersFirstnameStartingWithTest()
        {
            IEnumerable<Teacher> teachers = Context.Instance.TeacherRepo.FindTeachersFirstnameStartingWith("M");
            Assert.AreEqual(2, teachers.Count());

        }

        [TestMethod]
        public void FindMagazinesByAuthorTest()
        {
            IEnumerable<Magazine> magazines = Context.Instance.MagazineRepo.FindMagazinesByAuthor("y");
            Assert.AreEqual(0, magazines.Count());
        }

        [TestMethod]
        public void FindMagazinesBetweenPagesTest()
        {
            IEnumerable<Magazine> magazines = Context.Instance.MagazineRepo.FindMagazinesBetweenPages(10, 11);
            Assert.AreEqual(0, magazines.Count());
        }

        [TestMethod]
        public void FindBooksByAuthorTest()
        {
            Author a = Context.Instance.AuthorRepo.Find(1);
            IEnumerable<Book> books = Context.Instance.BookRepo.FindBooksByAuthor("a");
            Assert.AreEqual(3, books.Count());
        }

        [TestMethod]
        public void FindBookBetweenPagesTest()
        {
            IEnumerable<Book> books = Context.Instance.BookRepo.FindBookBetweenPages(10, 11);
            Assert.AreEqual(0, books.Count());
        }

        [TestMethod]
        public void FindBooksAllByThen()
        {
            IEnumerable<Book> books = Context.Instance.BookRepo.FindAllBooksOrderByTitleThenByPages();
            Assert.AreEqual(11, books.Count());
        }

        [TestMethod]
        public void FindBooksByTitleTest()
        {
            IEnumerable<Book> books = Context.Instance.BookRepo.FindBooksByTitle("a");
            Assert.AreEqual(3, books.Count());
        }

        [TestMethod]
        public void FindSchholByNameTest()
        {
            IEnumerable<School> sch = Context.Instance.SchoolRepo.FindSchoolByNameContains("HTL");
            Assert.AreEqual(2, sch.Count());
        }

        [TestMethod]
        public void FindBookByTitleTest()
        {
            IEnumerable<Book> book = Context.Instance.BookRepo.FindBookByTitleContains("Herr der Ringe");
            Assert.AreEqual(6, book.Count());
        }

        [TestMethod]
        public void FindBookByCategoryTest()
        {
            IEnumerable<Book> book = Context.Instance.BookRepo.FindBookByCategory("HISTORY");
            Assert.AreEqual(9, book.Count());
        }

        [TestMethod]
        public void FindBooksByCategoryTest()
        {
            Author a = Context.Instance.AuthorRepo.Find(1);
            IEnumerable<Book> books = Context.Instance.BookRepo.FindBooksByAuthor("a");
            Assert.AreEqual(3, books.Count());
        }

        [TestMethod]
        public void FindMagazinesByCategoryTest()
        {
            IEnumerable<Magazine> magazines = Context.Instance.MagazineRepo.FindMagazinesByAuthor("y");
            Assert.AreEqual(0, magazines.Count());
        }

        [TestMethod]
        public void FindSchholByNaemTest()
        {
            IEnumerable<School> sch = Context.Instance.SchoolRepo.FindSchoolByNameContains("HLW");
            Assert.AreEqual(2, sch.Count());
        }

        [TestMethod]
        public void FindStudentsOrderbyAge()
        {
            IEnumerable<Student> students = Context.Instance.StudentRepo.FindStudentsOrderByAge();
            Student[] std = students.Cast<Student>().ToArray();
            Assert.AreEqual(18, std[1].Age);
        }



    }
}
