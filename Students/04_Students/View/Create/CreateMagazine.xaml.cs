﻿using _04_Students.DB;
using _04_Students.Model.Article;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateMagazine.xaml
    /// </summary>
    public partial class CreateMagazine : Window
    {
        private List<Publisher> publisher = new List<Publisher>();

        public CreateMagazine()
        {
            InitializeComponent();
            List<Publisher> authorsSource = Context.Instance.PublisherRepo.ToList();
            cbPublisher.ItemsSource = authorsSource;
            cbCategory.ItemsSource = Enum.GetValues(typeof(ECategory)).Cast<ECategory>();
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            string title = txtTitle.Text;
            float price = Convert.ToSingle(txtPrice.Text);
            int pages = Convert.ToInt16(txtPages.Text);
            ECategory category = (ECategory)cbCategory.SelectedItem;

            Magazine b = new Magazine
            {
                Title = title,
                Price = price,
                Pages = pages,
                Category = category,
                Publishers = publisher
            };

            MagazineRepo.Instance.Add(b);        
            Close();
        }

        private void CbMagazines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Publisher a = (Publisher) cbPublisher.SelectedItem;
            if (!publisher.Contains(a))
            {
                publisher.Add(a);
                string lbl = lblPublishers.Content.ToString();
                lbl = lbl + a + ", ";
                lblPublishers.Content = lbl;
            }

        }
    }
}
