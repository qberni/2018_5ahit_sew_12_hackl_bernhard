﻿using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Person;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateTeacher.xaml
    /// </summary>
    public partial class CreateTeacher : Window
    {
        private List<Subject> subjects = new List<Subject>();

        public CreateTeacher()
        {
            InitializeComponent();
            cbSchool.ItemsSource = Context.Instance.SchoolRepo.ToList();
            cbSubjects.ItemsSource = Context.Instance.SubjectRepo.ToList();

        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (cbSchool.SelectedItem != null && subjects.Count != 0)
            {
                Teacher s = new Teacher
                {
                    Firstname = txtFirstname.Text,
                    Lastname = txtLastname.Text,
                    Age = Convert.ToInt32(txtAge.Text),
                    School = (School)cbSchool.SelectedItem,
                    Subjects = subjects
                };

                TeacherRepo.Instance.Add(s);
                Close();
            }
            else
            {
                MessageBox.Show("You have to select a school and min. 1 subject!");
            }
        }

        private void CbSubjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!subjects.Contains((Subject)cbSubjects.SelectedItem))
            {
                subjects.Add((Subject) cbSubjects.SelectedItem);
                string old = lblSubjects.Content.ToString();
                lblSubjects.Content = old + ", " + (Subject) cbSubjects.SelectedItem;
            } else
            {
                MessageBox.Show("You have already selected this subject!");
            }
        }
    }
}
