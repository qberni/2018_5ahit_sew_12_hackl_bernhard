﻿using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Person;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateOrder.xaml
    /// </summary>
    public partial class CreateOrder : Window
    {
        public CreateOrder()
        {
            InitializeComponent();

            List<APerson> persons = new List<APerson>();
            persons.AddRange(TeacherRepo.Instance.GetAll());
            persons.AddRange(StudentRepo.Instance.GetAll());
            personDataGrid.ItemsSource = persons;

            List<AArticle> articles = new List<AArticle>();
            articles.AddRange(BookRepo.Instance.GetAll());
            articles.AddRange(MagazineRepo.Instance.GetAll());
            articleDataGrid.ItemsSource = articles;

        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (articleDataGrid.SelectedItem != null && personDataGrid.SelectedItem != null)
            {
                Order o = new Order
                {
                    Article = (AArticle)articleDataGrid.SelectedItem,
                    Person = (APerson)personDataGrid.SelectedItem
                };
                Context.Instance.OrderRepo.Add(o);
                Context.Instance.SaveChanges();
                Close();
            }
            else
            {
                MessageBox.Show("You have to select one item of each box!");
            }
        }
    }
}
