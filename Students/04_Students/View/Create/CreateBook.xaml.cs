﻿using _04_Students.DB;
using _04_Students.Model.Article;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateBook.xaml
    /// </summary>
    public partial class CreateBook : Window
    {
        private List<Author> authors = new List<Author>();
        
        public CreateBook()
        {
            InitializeComponent();
            List<Author> authorsSource = Context.Instance.AuthorRepo.ToList();
            cbAuthors.ItemsSource = authorsSource;
            cbCategory.ItemsSource = Enum.GetValues(typeof(ECategory)).Cast<ECategory>();
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            string title = txtTitle.Text;
            float price = Convert.ToSingle(txtPrice.Text);
            int pages = Convert.ToInt16(txtPages.Text);
            string isbn = txtIsbn.Text;
            ECategory category = (ECategory) cbCategory.SelectedItem;

            Book b = new Book
            {
                Title = title,
                Price = price,
                Pages = pages,
                Category = category,
                Isbn = isbn,
                Authors = authors
            };

            BookRepo.Instance.Add(b);
            Close();
        }

        private void CbAuthors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Author a = (Author) cbAuthors.SelectedItem;
            if (!authors.Contains(a))
            {
                authors.Add(a);
                string lbl = lblAuthors.Content.ToString();
                lbl = lbl + a + ", ";
                lblAuthors.Content = lbl;
            }
            
        }
    }
}
