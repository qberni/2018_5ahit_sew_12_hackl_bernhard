﻿using _04_Students.DB;
using _04_Students.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateSchool.xaml
    /// </summary>
    public partial class CreateSchool : Window
    {
        public CreateSchool()
        {
            InitializeComponent();
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            string name = txtSchoolName.Text;
            if(name != "")
            {
                School s = new School
                {
                    Name = name
                };
                Context.Instance.SchoolRepo.Add(s);
                Context.Instance.SaveChanges();
                Close();
            } else
            {
                MessageBox.Show("You have to enter a name");
            }

           
        }
    }
}
