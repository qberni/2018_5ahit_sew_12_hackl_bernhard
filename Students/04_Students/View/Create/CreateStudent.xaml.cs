﻿using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Person;
using _04_Students.Repository;
using _04_Students.ViewModel.CommandVm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateStudent.xaml
    /// </summary>
    public partial class CreateStudent : Window
    {
        public CreateStudent()
        {
            InitializeComponent();
            cbSchool.ItemsSource = Context.Instance.SchoolRepo.ToList();
            DataContext = this;
            
        }

        public ICommand CreateCommand
        {
            get { return new RelayCommand(a => Create()); }
        }

        public void Create()
        {

            if (cbSchool.SelectedItem != null)
            {
                Student s = new Student
                {
                    Firstname = txtFirstname.Text,
                    Lastname = txtLastname.Text,
                    Age = Convert.ToInt32(txtAge.Text),
                    School = (School)cbSchool.SelectedItem
                };

                StudentRepo.Instance.Add(s);
                Close();
            }
            else
            {
                MessageBox.Show("You have to select a school!");
            }
        }
    }
}
