﻿using _04_Students.DB;
using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Create
{
    /// <summary>
    /// Interaktionslogik für CreateAuthor.xaml
    /// </summary>
    public partial class CreateAuthor : Window
    {
        public CreateAuthor()
        {
            InitializeComponent();
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name.Length != 0)
            {
                Author a = new Author(txtName.Text, null);
                Context.Instance.AuthorRepo.Add(a);
                Context.Instance.SaveChanges();
                Close();
            }
        }
    }
}
