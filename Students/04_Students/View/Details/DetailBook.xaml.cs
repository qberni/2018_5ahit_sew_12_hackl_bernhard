﻿using _04_Students.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _04_Students.View.Details
{
    /// <summary>
    /// Interaktionslogik für DetailBook.xaml
    /// </summary>
    public partial class DetailBook : Window
    {
        public BookViewModel viewModel = BookViewModel.Instance;

        public DetailBook()
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }
    }
}
