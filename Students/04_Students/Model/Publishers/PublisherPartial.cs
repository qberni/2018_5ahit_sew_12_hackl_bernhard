﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class Publisher
    {
        public Publisher()
        {

        }
        public Publisher(string name, ICollection<Magazine> magazines) : base(name)
        {
            this.Magazines = magazines;
        }
    }
}
