﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class APublisher
    { 
        public APublisher()
        {

        }
        public APublisher(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
