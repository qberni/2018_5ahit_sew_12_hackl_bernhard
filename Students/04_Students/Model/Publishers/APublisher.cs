﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class APublisher : AEntity
    {
        [StringLength(100)]
        public string Name { get; set; }
    }
}
