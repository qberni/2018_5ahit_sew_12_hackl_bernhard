﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class Author : APublisher
    {
        public ICollection<Book> Books { get; set; }
    }
}
