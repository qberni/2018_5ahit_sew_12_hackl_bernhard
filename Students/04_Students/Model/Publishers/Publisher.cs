﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class Publisher : APublisher
    {
        public ICollection<Magazine> Magazines { get; set; }
    }
}
