﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Publishers
{
    public partial class Author
    {
        public Author()
        {
            this.Books = new List<Book>();
        }

        public Author(string name, ICollection<Book> books) : base(name)
        {
            if (books != null)
                this.Books = books;
            else
                this.Books = new List<Book>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
