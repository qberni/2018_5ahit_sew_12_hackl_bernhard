﻿using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _04_Students.Model.Person;

namespace _04_Students.Model
{
    public partial class Order : AEntity
    {
        [Column("personId")]
        public APerson Person { get; set; }

        [Column("article")]
        public AArticle Article { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
