﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    public partial class AArticle : AEntity
    {
        public string Title { get; set; }

        public float Price { get; set; }

        public int Pages { get; set; }

        public ECategory Category { get; set; }
    }


}
