﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    [Table("Books")]
    public partial class Book : AArticle
    {
        public string Isbn { get; set; }

        public ICollection<Author> Authors { get; set; }
    }
}
