﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    public partial class Magazine
    {
        public Magazine()
        {

        }
        public Magazine(string title, float price, int pages, ICollection<Publisher> publisher, ECategory category) : base(title, price, pages, category)
        {
            this.Publishers = publisher;
        }

    }
}
