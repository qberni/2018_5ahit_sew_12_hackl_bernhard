﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    [Table("Magazines")]
    public partial class Magazine : AArticle
    {
        public ICollection<Publisher> Publishers { get; set; }
    }
}
