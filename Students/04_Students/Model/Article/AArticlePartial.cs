﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    public partial class AArticle
    {
        public AArticle()
        {

        }
        public AArticle(string title, float price, int pages, ECategory category)
        {
            this.Title = title;
            this.Pages = pages;
            this.Price = price;
            this.Category = category;
        }

        public override string ToString()
        {
            return Title;
        }
    }

    public enum ECategory { TECH, HISTORY, LOVE, ANIMALS, SCHOOL }
}
