﻿using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Article
{
    public partial class Book
    {
        public Book()
        {

        }

        public Book(string title, float price, int pages, ICollection<Author> authors, ECategory category, string isbn) : base(title, price, pages, category)
        {
            this.Isbn = isbn;
            this.Authors = authors;
        }
    }
}
