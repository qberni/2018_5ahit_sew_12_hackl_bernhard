﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model
{
    public partial class School
    {
        public School()
        {

        }

        public School(string name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }


    }
}
