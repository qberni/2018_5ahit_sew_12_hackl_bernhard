﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model
{
    public abstract class AEntity
    {
        [Key]
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            var entity = obj as AEntity;
            return entity != null &&
                   Id == entity.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }
    }
}
