﻿using System.ComponentModel.DataAnnotations;

namespace _04_Students.Model.Person
{
    public abstract partial class APerson : AEntity
    {
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Lastname { get; set; }

        public int Age { get; set; }
    }
}
