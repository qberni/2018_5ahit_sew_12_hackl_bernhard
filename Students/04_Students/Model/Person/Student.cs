﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace _04_Students.Model.Person
{
    [Table("Student")]
    public partial class Student : APerson
    {
        public School School { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
