﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Person
{
    public partial class Teacher
    {
        public Teacher()
        {

        }
        public Teacher(string firstname, string lastname, int age, School school, ICollection<Order> orders, ICollection<Subject> subjects) : base(firstname, lastname, age)
        {
            this.School = school;
            this.Orders = orders;
            this.Subjects = subjects;
        }
    }
}
