﻿using System.Collections;
using System.Collections.Generic;

namespace _04_Students.Model.Person
{
    public partial class Subject : AEntity
    {
        public string Name { get; set; }

        public ICollection<Teacher> Teachers { get; set; }
    }
}
