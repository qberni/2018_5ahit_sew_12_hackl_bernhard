﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Person
{
    public partial class Student
    {
        public Student()
        {

        }
        public Student(string firstname, string lastname, int age, School school) : base(firstname, lastname, age)
        {
            this.School = school;
        }
    }
}
