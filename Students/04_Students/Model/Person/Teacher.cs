﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace _04_Students.Model.Person
{
    [Table("Teacher")]
    public partial class Teacher : APerson
    {
        public School School { get; set; }

        public ICollection<Order> Orders { get; set; }

        public ICollection<Subject> Subjects { get; set; }
    }
}
