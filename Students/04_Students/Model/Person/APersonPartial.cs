﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model.Person
{
    public partial class APerson
    {
        public APerson()
        {

        }
        public APerson(string firstname, string lastname, int age)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
        }

        public override string ToString()
        {
            return Firstname + " " + Lastname;
        }
    }
}
