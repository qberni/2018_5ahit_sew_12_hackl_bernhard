﻿using _04_Students.Model.Article;
using _04_Students.Model.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Model
{
    public partial class Order
    {
        public Order()
        {

        }
        public Order(APerson person, AArticle article)
        {
            this.Article = article;
            this.Person = person;
        }
    }
}
