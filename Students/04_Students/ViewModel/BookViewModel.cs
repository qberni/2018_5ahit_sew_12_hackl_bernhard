﻿using _04_Students.DB;
using _04_Students.Model.Article;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.ViewModel
{
    public class BookViewModel : AViewModel
    {
        public Book Book { get; set; } = new Book();
        public static BookViewModel Instance { get; } = new BookViewModel();

        public BindingList<Book> List { get; set; } = new BindingList<Book>();

        public void LoadData() => List = Context.Instance.BookRepo.Local.ToBindingList();
        
        public string Isbn
        {
            get { return Book.Isbn; }
            set { Book.Isbn = value; this.NotifyPropertyChanged(); }
        }

        public int Id
        {
            get { return Book.Id; }      
        }

        public int Pages
        {
            get { return Book.Pages; }
            set { Book.Pages = value; this.NotifyPropertyChanged(); }
        }

        public float Price
        {
            get { return Book.Price; }
            set { Book.Price = value; this.NotifyPropertyChanged(); }
        }

        public string Title
        {
            get { return Book.Title; }
            set { Book.Title = value; this.NotifyPropertyChanged(); }
        }

        public ECategory Category
        {
            get { return Book.Category; }
            set { Book.Category = value; this.NotifyPropertyChanged(); }
        }

        public ObservableCollection<Author> Publishers
        {
            get { return new ObservableCollection<Author>(Book.Authors); }
        }
       

        private BookViewModel()
        {
            LoadData();
        }
    }
}
