﻿using _04_Students.DB;
using _04_Students.Model.Article;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.ViewModel
{
    public class MagazineViewModel : AViewModel
    {
        public static MagazineViewModel Instance { get; } = new MagazineViewModel();

        public BindingList<Magazine> List { get; set; } = new BindingList<Magazine>();

        public void LoadData() => List = Context.Instance.MagazineRepo.Local.ToBindingList();
        

        private MagazineViewModel()
        {
            LoadData();
        }
        
    }
}
