﻿using _04_Students.DB;
using _04_Students.Model.Person;
using _04_Students.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.ViewModel
{
    public class TeacherViewModel : AViewModel
    {
        public BindingList<Teacher> List { get; set; } = new BindingList<Teacher>();

        public static TeacherViewModel Instance { get; } = new TeacherViewModel();

        public void LoadData() => List = Context.Instance.TeacherRepo.Local.ToBindingList();

        private TeacherViewModel()
        {
            LoadData();
        }
        
    }
}
