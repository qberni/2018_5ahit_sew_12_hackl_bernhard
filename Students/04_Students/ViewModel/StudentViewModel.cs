﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using _04_Students.Model.Person;
using System.ComponentModel;
using _04_Students.Repository;
using _04_Students.DB;
using System.Data.Entity;

namespace _04_Students.ViewModel
{
    public class StudentViewModel : AViewModel
    {
        public BindingList<Student> List { get; set; } = new BindingList<Student>();
        public Student s; //Properties hinzufügen

        public void LoadData() => List = Context.Instance.StudentRepo.Local.ToBindingList();

        public static StudentViewModel Instance { get; set;}= new StudentViewModel();
        private StudentViewModel()
        {
            LoadData();
        }


    }
}
