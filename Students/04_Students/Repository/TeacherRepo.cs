﻿using _04_Students.DB;
using _04_Students.Model.Person;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public class TeacherRepo : ARepository<Teacher>
    {
        private DbSet<Teacher> teachers;

        public static TeacherRepo Instance { get; } = new TeacherRepo();
        private TeacherRepo()
        {
            teachers = Context.Instance.TeacherRepo;
            table = teachers;
        }
    }
}
