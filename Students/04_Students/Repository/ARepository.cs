﻿using _04_Students.DB;
using _04_Students.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public abstract class ARepository<T> : IRepository<T> where T : AEntity
    {
        protected DbSet<T> table;
        private Context context = Context.Instance;

        public void Add(T entity)
        {
            table.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            context.Entry<T>(entity).State = EntityState.Modified;
            context.SaveChanges();
        }
        public void AddRange(IList<T> entities)
        {
            table.AddRange(entities);
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            table.Remove(entity);
            context.SaveChanges();
        }

        public void DeleteById(int? id)
        {
            context.Entry<T>(table.Find(id)).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public List<T> ExecuteQuery(string sql)
        {
            return table.SqlQuery(sql).ToList();
        }

        public List<T> ExecuteQuery(string sql, object[] sqlParametersObjects)
        {
            return table.SqlQuery(sql, sqlParametersObjects).ToList();
        }

        public List<T> GetAll()
        {
            return table.ToList();
        }

        public T GetOne(int? id)
        {
            return table.Find(id);
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
