﻿using _04_Students.DB;
using _04_Students.Model.Person;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public class StudentRepo : ARepository<Student>
    {
        private DbSet<Student> students;
        public static StudentRepo Instance { get; } = new StudentRepo();
        private StudentRepo()
        {
            students = Context.Instance.StudentRepo;
            table = students;
        }
    }
}
