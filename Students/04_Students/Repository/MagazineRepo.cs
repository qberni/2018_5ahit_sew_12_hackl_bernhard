﻿using _04_Students.DB;
using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public class MagazineRepo : ARepository<Magazine>
    {
        private DbSet<Magazine> magazines;

        public static MagazineRepo Instance { get; } = new MagazineRepo();
        private MagazineRepo()
        {
            magazines = Context.Instance.MagazineRepo;
            table = magazines;
        }
    }
}
