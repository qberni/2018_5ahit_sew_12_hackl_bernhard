﻿using _04_Students.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public interface IRepository<T> where T : AEntity
    {
        void Add(T entity);
        void AddRange(IList<T> entities);
        void Save();
        void Update(T entity);
        void DeleteById(int? id);
        void Delete(T entity);
        T GetOne(int? id);
        List<T> GetAll();   
        List<T> ExecuteQuery(string sql);
        List<T> ExecuteQuery(string sql, object[] sqlParametersObjects);
    }
}
