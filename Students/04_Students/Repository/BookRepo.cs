﻿using _04_Students.DB;
using _04_Students.Model.Article;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public class BookRepo : ARepository<Book>
    {
        private DbSet<Book> books;
        public static BookRepo Instance { get; } = new BookRepo();
        private BookRepo()
        {
            books = Context.Instance.BookRepo;
            table = books;
        }

    }
}
