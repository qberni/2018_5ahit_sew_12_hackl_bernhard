﻿using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Person;
using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Students.Repository
{
    public static class Extensions
    {
        //Student extensions
        public static IEnumerable<Student> FindStudentsBetweenAge(this DbSet<Student> students, int lowerBound, int upperBound)
        {
            return students.Where(a => a.Age >= lowerBound && a.Age <= upperBound);
           
        }

        public static IEnumerable<Student> FindStudentsWithAge(this DbSet<Student> students, int value)
        {
            return students.Where(a => a.Age == value);
        }

        public static IEnumerable<Student> FindStudentsWithLastNameContains(this DbSet<Student> students, string value)
        {
            return students.Where(a => a.Lastname.Contains(value));
        }

        public static IEnumerable<Student> FindStudentsOrderByAge(this DbSet<Student> students)
        {
            return students.OrderBy(a => a.Age);
        }

        public static IEnumerable<Student> FindStudentsOrderByMaxAge(this DbSet<Student> students)
        {
            return students.Where(a => a.Age == 22);
        }


        public static IEnumerable<Student> FindStudentsBySchool(this DbSet<Student> students, School s)
        {
            DbSet<School> schools = Context.Instance.SchoolRepo;
            var result = students.Join(schools, student => student.Id, school => school.Id, (student, school) => new Student
            {
                Id = student.Id,
                School = school,
                Age = student.Age,
                Firstname = student.Firstname,
                Lastname = student.Lastname,
                Orders = student.Orders
            }).Where(a => a.School == s);

            return result;
        }

        //Teacher extensions
        public static IEnumerable<Teacher> FindTeachersBySubject(this DbSet<Teacher> teachers, Subject s)
        {
            return teachers.Where(a => a.Subjects.Contains(s));
        }

        public static IEnumerable<Teacher> FindTeachersFirstnameStartingWith(this DbSet<Teacher> teachers, string token)
        {
            return teachers.Where(a => a.Firstname.StartsWith(token));
        }

        //Magazine extensions
        public static IEnumerable<Magazine> FindMagazinesByAuthor(this DbSet<Magazine> magazines, string value)
        {
            return magazines.Where(b => b.Title.Contains(value));
        }

        public static IEnumerable<Magazine> FindMagazinesBetweenPages(this DbSet<Magazine> magazines, int lowerBound, int upperBound)
        {
            return magazines.Where(a => a.Pages <= upperBound && a.Pages >= lowerBound);
        }

        //Book extensions
        public static IEnumerable<Book> FindBooksByAuthor(this DbSet<Book> books, string a)
        {
            return books.Where(b => b.Title.Contains(a));
        }

        public static IEnumerable<Book> FindBookBetweenPages(this DbSet<Book> books, int lowerBound, int upperBound)
        {
            return books.Where(a => a.Pages <= upperBound && a.Pages >= lowerBound);
        }

        public static IEnumerable<Book> FindAllBooksOrderByTitleThenByPages(this DbSet<Book> books)
        {
            return books.OrderBy(s => s.Title).ThenBy(s => s.Pages);
        }

        public static IEnumerable<Book> FindBooksByTitle(this DbSet<Book> books, string a)
        {
            return books.Where(b => b.Title.Contains(a));
        }

        public static IEnumerable<School> FindSchoolByNameContains(this DbSet<School> school, string a)
        {
            return school.Where(b => b.Name.Contains(a));
        }

        public static IEnumerable<Book> FindBookByTitleContains(this DbSet<Book> book, string a)
        {
            return book.Where(b => b.Title.Contains(a));
        }

        public static IEnumerable<Book> FindBookByCategory(this DbSet<Book> book, string a)
        {
            return book.Where(b => b.Category.ToString() == a);
        }

        public static IEnumerable<Student> FindStudentByFirstAndLAst(this DbSet<Student> student, string first, string last)
        {
            return student.Where(s => s.Firstname == first && s.Lastname == last);
        }









    }
}
