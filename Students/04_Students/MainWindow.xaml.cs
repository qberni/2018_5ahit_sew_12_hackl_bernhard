﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _04_Students.DB;
using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Person;
using _04_Students.Model.Publishers;
using _04_Students.Repository;
using _04_Students.View.Create;
using _04_Students.View.Details;
using _04_Students.ViewModel;
using _04_Students.ViewModel.CommandVm;

namespace _04_Students
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public BookViewModel bookViewModel = BookViewModel.Instance;
        public StudentViewModel studentViewModel = StudentViewModel.Instance;
        public MagazineViewModel magazineViewModel = MagazineViewModel.Instance;
        public TeacherViewModel teacherViewModel = TeacherViewModel.Instance;

        private BookRepo bookRepo = BookRepo.Instance;
        private TeacherRepo teacherRepo = TeacherRepo.Instance;
        private MagazineRepo magazineRepo = MagazineRepo.Instance;
        private StudentRepo studentRepo = StudentRepo.Instance;
        
        private Context c = Context.Instance;
        private bool help = false;

        public MainWindow()
        {
            InitializeComponent();
            LoadDbData();
            mainDataGrid.ItemsSource = studentViewModel.List;
            DataContext = this;
        }

        private void LoadDbData()
        {
            c.PublisherRepo.Load();
            c.AuthorRepo.Load();
            c.BookRepo.Load();
            c.MagazineRepo.Load();
            c.StudentRepo.Load();
            c.TeacherRepo.Load();
            c.OrderRepo.Load();
            c.SchoolRepo.Load();
        }

        private void CbSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (help)
            {
                CheckCurrentSource();
            }
            help = true;
        }
        /*
        public void CheckCurrentSource() {
            switch (cbSource.SelectedValue.ToString().ToLower())
            {
                case "student": mainDataGrid.ItemsSource = studentViewModel.List; break;
                case "order": mainDataGrid.ItemsSource = c.OrderRepo.Local.ToBindingList(); break;
                case "teacher": mainDataGrid.ItemsSource = teacherViewModel.List; break;
                case "publisher": mainDataGrid.ItemsSource = c.PublisherRepo.Local.ToBindingList(); break;
                case "school": mainDataGrid.ItemsSource = c.SchoolRepo.Local.ToBindingList(); break;
                case "magazine": mainDataGrid.ItemsSource = magazineViewModel.List; break;
                case "book": mainDataGrid.ItemsSource = bookViewModel.List; break;
                case "author": mainDataGrid.ItemsSource = c.AuthorRepo.Local.ToBindingList(); break;
            }
        }  
        */
        private void MainDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Publishers": e.Column = null; break;
                case "School": e.Column = null; break;
                case "Orders": e.Column = null; break;
                case "Subjects": e.Column = null; break;
                case "Books": e.Column = null; break;
                case "Students": e.Column = null; break;
                case "Teachers": e.Column = null; break;
                case "Magazines": e.Column = null; break;
            }
        }

        /*
        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            switch (cbSource.SelectedValue.ToString().ToLower())
            {
                case "student":
                    CreateStudent cStudent = new CreateStudent();
                    cStudent.Show();
                    break;
                case "order":
                    CreateOrder createOrder = new CreateOrder();
                    createOrder.Show();
                    break;
                case "teacher":
                    CreateTeacher createTeacher = new CreateTeacher();
                    createTeacher.Show();
                    break;
                case "publisher":
                    CreatePublisher createPublisher = new CreatePublisher();
                    createPublisher.Show();
                    break;
                case "school":
                    CreateSchool createSchool = new CreateSchool();
                    createSchool.Show();
                    break;
                case "magazine":
                    CreateMagazine createMagazine = new CreateMagazine();
                    createMagazine.Show();
                    break;
                case "book":
                    CreateBook createBook = new CreateBook();
                    createBook.Show();
                    break;
                case "author":
                    CreateAuthor createAuthor = new CreateAuthor();
                    createAuthor.Show();
                    break;
            }
        }

        private void BtnDetails_Click(object sender, RoutedEventArgs e)
        {
            switch (cbSource.SelectedValue.ToString().ToLower())
            {
                case "student":
                    CreateStudent cStudent = new CreateStudent();
                    cStudent.Show();
                    break;
                case "order":
                    CreateOrder createOrder = new CreateOrder();
                    createOrder.Show();
                    break;
                case "teacher":
                    CreateTeacher createTeacher = new CreateTeacher();
                    createTeacher.Show();
                    break;
                case "publisher":
                    CreatePublisher createPublisher = new CreatePublisher();
                    createPublisher.Show();
                    break;
                case "school":
                    CreateSchool createSchool = new CreateSchool();
                    createSchool.Show();
                    break;
                case "magazine":
                    CreateMagazine createMagazine = new CreateMagazine();
                    createMagazine.Show();
                    break;
                case "book":
                    DetailBook details = new DetailBook();
                    BookViewModel.Instance.Book = (Book) mainDataGrid.SelectedValue;
                    details.Show();
                    break;
                case "author":
                    Author a = (Author)mainDataGrid.SelectedItem;
                    
                    Author d = c.AuthorRepo.Where(b => b.Id == a.Id).Include("Books").Single();
                    DetailAuthor detailAuthor = new DetailAuthor(d);
                    detailAuthor.Show();
                    break;
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Context c = Context.Instance;
            if (mainDataGrid.SelectedItem != null)
            {
                switch (cbSource.SelectedValue.ToString().ToLower())
                {
                    case "student":
                        StudentRepo.Instance.Delete((Student)mainDataGrid.SelectedItem);
                        break;
                    case "order":
                        c.OrderRepo.Remove((Order)mainDataGrid.SelectedItem);
                        c.SaveChanges();
                        break;
                    case "teacher":
                        TeacherRepo.Instance.Delete((Teacher)mainDataGrid.SelectedItem);
                        break;
                    case "publisher":
                        c.PublisherRepo.Remove((Publisher)mainDataGrid.SelectedItem);
                        c.SaveChanges();
                        break;
                    case "school":
                        c.SchoolRepo.Remove((School)mainDataGrid.SelectedItem);
                        c.SaveChanges();
                        break;
                    case "magazine":
                        MagazineRepo.Instance.Delete((Magazine)mainDataGrid.SelectedItem);
                        break;
                    case "book":
                        BookRepo.Instance.Delete((Book)mainDataGrid.SelectedItem);
                        break;
                    case "author":
                        c.AuthorRepo.Remove((Author)mainDataGrid.SelectedItem);
                        c.SaveChanges();
                        break;
                }
            }

        }
        */
        public ICommand AddCommand
        {
            get { return new RelayCommand(a => Add()); }
        }

        public void Add()
        {
            CreateStudent cStudent = new CreateStudent();
            cStudent.Show();
        }

        public ICommand DeleteCommand
        {
            get { return new RelayCommand(a => Delete()); }
        }

        public void Delete()
        {
            StudentRepo.Instance.Delete((Student)mainDataGrid.SelectedItem);
            c.SaveChanges();
        }

        public ICommand UpdateCommand
        {
            get { return new RelayCommand(a => Update()); }
        }

        private void Update()
        {
            var std = studentViewModel.s;
            studentRepo.Add(std);
            std = null;
        }
    }
}
