﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Person;
using _04_Students.Model.Publishers;

namespace _04_Students.DB
{
    public class LibraryDbInitializer : DropCreateDatabaseAlways<Context>
    {
        protected override void Seed(Context context)
        {


            var subjects = new List<Subject>();
            subjects.Add(new Subject("History"));
            subjects.Add(new Subject("Math"));
            subjects.Add(new Subject("German"));
            subjects.Add(new Subject("English"));
            subjects.Add(new Subject("Sport"));
            subjects.Add(new Subject("Art"));
            subjects.Add(new Subject("Software Engineering"));
            subjects.Add(new Subject("INSY"));
            subjects.Add(new Subject("SYTI"));
            subjects.Add(new Subject("SYTS"));
            subjects.Add(new Subject("SYTD"));
            subjects.Add(new Subject("SYTB"));
            context.SubjectRepo.AddRange(subjects);

            var schools = new List<School>()
            {
                new School("HTL Krems"),
                new School("HAK Horn"),
                new School("HLW/HLM Krems"),
                new School("HLW Horn"),
                new School("BORG Krems"),
                new School("HTL Hollabrunn")
            };
            context.SchoolRepo.AddRange(schools);


            var students = new List<Student>();
            students.Add(new Student("Alexander", "Kauer", 18, schools[0]));
            students.Add(new Student("Christian", "Lauer", 22, schools[0]));
            students.Add(new Student("Birgit", "Zeller", 18, schools[1]));
            students.Add(new Student("Kevin", "Zach", 19, schools[1]));
            students.Add(new Student("Michael", "Apfelthaler", 19, schools[1]));
            students.Add(new Student("Dominik", "Dafert", 18, schools[1]));
            context.StudentRepo.AddRange(students);

            var teacherSubj = new List<Subject>()
            {
                subjects[0],
                subjects[2],
                subjects[3]
            };
           Thread.Sleep(1000);

            var teachers = new List<Teacher>()
            {
                new Teacher("Max", "Mustermann", 33, null, null, teacherSubj),
                new Teacher("Jürgen", "Hauptmann", 33, null, null, null),
                new Teacher("Paul", "Panhofer", 33, null, null, null),
                new Teacher("Markus", "Brunner", 33, null, null, null)
            };

            context.TeacherRepo.AddRange(teachers);

            var books = new List<Book>();
            books.Add(new Book("Herr der Ringe 1", 10.0f, 100, null, ECategory.HISTORY, "1234567890"));
            books.Add(new Book("Herr der Ringe 2", 10.0f, 100, null, ECategory.HISTORY, "1234567890a"));
            books.Add(new Book("Herr der Ringe 3", 10.0f, 100, null, ECategory.HISTORY, "123456789aa0"));
            books.Add(new Book("Herr der Ringe 4", 10.0f, 100, null, ECategory.HISTORY, "1234sss5678a90"));
            books.Add(new Book("Herr der Ringe 5", 10.0f, 100, null, ECategory.HISTORY, "345678543"));
            books.Add(new Book("Harry Potter", 10.0f, 100, null, ECategory.HISTORY, "r78876545678"));
            books.Add(new Book("Java ist eine Insel", 10.0f, 100, null, ECategory.TECH, "123333333"));
            books.Add(new Book("Lombok ist auch eine Insel", 10.0f, 100, null, ECategory.TECH, "1234123567890"));
            books.Add(new Book("1. WK", 10.0f, 100, null, ECategory.HISTORY, "1234444444"));
            books.Add(new Book("2. WK", 10.0f, 100, null, ECategory.HISTORY, "1231231234567890"));
            books.Add(new Book("Herr der Ringe 1", 10.0f, 100, null, ECategory.HISTORY, "1234512312367890"));
            context.BookRepo.AddRange(books);

            var magazines = new List<Magazine>();
            magazines.Add(new Magazine("Golem", 2.0f, 20, null, ECategory.TECH));
            magazines.Add(new Magazine("1. Republik", 2.0f, 15, null, ECategory.HISTORY));
            context.MagazineRepo.AddRange(magazines);


            var authors = new List<Author>()
            {
                new Author("JRK Tolkin", books),
                new Author("Kafkar", null),
                new Author("Susan Collins", null),
                new Author("keine Ahngung", null)
            };
            context.AuthorRepo.AddRange(authors);

            var magazinesAuthors = new List<Magazine>()
            {
                magazines[1],
                magazines[0]
            };

            var publisher = new List<Publisher>()
            {
                new Publisher("Bernhard Steindl", magazinesAuthors),
                new Publisher("Bernhard Hackl", null)
            };
            context.PublisherRepo.AddRange(publisher);


            var orders = new List<Order>()
            {
                new Order(teachers[0], books[2]),
                new Order(students[1], magazines[0]),
                new Order(teachers[3], books[1])
            };
            context.OrderRepo.AddRange(orders);

            context.SaveChanges();
        }
    }
}
