﻿using _04_Students.Model;
using _04_Students.Model.Article;
using _04_Students.Model.Publishers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _04_Students.Model.Person;

namespace _04_Students.DB
{
    public class Context : DbContext
    {
        public DbSet<Book> BookRepo { get; set; }
        public DbSet<Magazine> MagazineRepo { get; set; }

        public DbSet<Student> StudentRepo { get; set; }
        public DbSet<Teacher> TeacherRepo { get; set; }
        public DbSet<Subject> SubjectRepo { get; set; }

        public DbSet<Publisher> PublisherRepo { get; set; }
        public DbSet<Author> AuthorRepo { get; set; }

        public DbSet<Order> OrderRepo { get; set; }
        public DbSet<School> SchoolRepo { get; set; }

        public static Context Instance { get; } = new Context();

        private Context() : base("Library_DB")
        {
            Database.SetInitializer(new LibraryDbInitializer());
            AuthorRepo.Include("Books");
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        
    }
}
