﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SongExample
{
    class SongViewmodel : INotifyPropertyChanged 
    {
        Song song;

        public SongViewmodel()
        {
            song = new Song { ArtistName = "Unknown", SongTitle = "Unknown"};
        }

        public Song Song
        {
            get { return song; }
            set { song = value; }
        }

        public string ArtistName
        {
            get { return Song.ArtistName; }
            set {if (Song.ArtistName != value)
                { Song.ArtistName = value;
   
                }

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
