﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SongExample
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SongViewmodel songViewmodel = new SongViewmodel();
        int count = 0; 

        public MainWindow()
        {
            InitializeComponent();
            base.DataContext = songViewmodel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            count++;
            songViewmodel.ArtistName = string.Format("Elvis {0}",count);
        }
    }
}
