﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SongExample
{
    class Song
    {
        string artistName;
        string songtitle;

        public string ArtistName
        {
            get { return artistName; }
            set { artistName = value; }
        }

        public string SongTitle
        {
            get { return songtitle; }
            set { songtitle = value; }
        }


    }
}
