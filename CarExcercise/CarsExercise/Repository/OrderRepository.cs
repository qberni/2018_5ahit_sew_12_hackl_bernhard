﻿using CarsExercise.Context;
using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Repository
{
    class OrderRepository : ARepository<Order>
    {
        public OrderRepository()
        {
        }

        public OrderRepository(CarDBContext ctx) : base(ctx)
        {
        }

        public IEnumerable<Order> Get()
        {
            var result = context.Orders.ToList();

            return result;
        }

        public ObservableCollection<Order> GetObservable()
        {
            var list = Get();
            var oc = new ObservableCollection<Order>();
            foreach (var item in list)
                oc.Add(item);

            return oc;
        }
    }
}
