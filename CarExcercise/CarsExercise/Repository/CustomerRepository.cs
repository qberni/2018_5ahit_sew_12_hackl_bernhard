﻿using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using CarsExercise.Context;

namespace CarsExercise.Repository
{
    class CustomerRepository : ARepository<Customer>
    {
        public CustomerRepository()
        {
        }

        public CustomerRepository(CarDBContext ctx) : base(ctx)
        {
        }
        public IEnumerable<Customer> Get()
        {
            var result = context.Customers.ToList();

            return result;
        }

        public ObservableCollection<Customer> GetObservable()
        {
            var list = Get();
            var oc = new ObservableCollection<Customer>();
            foreach (var item in list)
                oc.Add(item);

            return oc;
        }

        public Customer GetOne(int id)
        {
            var result = context.Customers.Where(x => x.CustId == id).Single();
            if(result == null)
            {
                return new Customer();
            }
            return result;
        }

  
    }
}
