﻿using CarsExercise.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CarsExercise.Repository
{
    abstract class ARepository<T> : IDisposable where T : class
    {
        protected CarDBContext context;

        public ARepository() { }
        public ARepository(CarDBContext ctx)
        {
            context = ctx;
        }
        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }
        public virtual async void Delete(T entity)
        {
            using (var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Set<T>().Remove(entity);
                await ctx.SaveChangesAsync();
            }
        }
        public virtual async void Create(T entity)
        {
            using(var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Set<T>().Add(entity);
                await ctx.SaveChangesAsync();
            }
         
        }
        public virtual async void Update(T entity)
        {
            using (var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Entry(entity).State = EntityState.Modified;
                await ctx.SaveChangesAsync();
            }
        }
        public virtual void DeleteSync(T entity)
        {
            using (var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Set<T>().Remove(entity);
                ctx.SaveChanges();
            }
        }
        public virtual void CreateSync(T entity)
        {
            using (var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Set<T>().Add(entity);
                ctx.SaveChanges();
            }

        }
        public virtual void UpdateSync(T entity)
        {
            using (var ctx = new CarDBContext())
            {
                ctx.Set<T>().Attach(entity);
                ctx.Entry(entity).State = EntityState.Modified;
                ctx.SaveChanges();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
           
        }
    }
}
