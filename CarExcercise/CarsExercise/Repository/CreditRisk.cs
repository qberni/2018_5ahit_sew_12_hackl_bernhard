﻿using CarsExercise.Context;
using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Repository
{
    class CreditRiskRepository : ARepository<CreditRisk>
    {
        public CreditRiskRepository()
        {
        }

        public CreditRiskRepository(CarDBContext ctx) :base(ctx)
        {
     
        }

        public IEnumerable<CreditRisk> Get()
        {
            var result = context.CreditRisks.ToList();

            return result;
        }

        public ObservableCollection<CreditRisk> GetObservable()
        {
            var list = Get();
            var oc = new ObservableCollection<CreditRisk>();
            foreach (var item in list)
                oc.Add(item);

            return oc;
        }

        public CreditRisk GetOne(int id)
        {
            var result = context.CreditRisks.Where(x => x.CustId == id).Single();

            return result;
        }

    }
}
