﻿using CarsExercise.Context;
using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace CarsExercise.Repository
{
    class CarRepository : ARepository<Car>
    {
        public CarRepository()
        {
        }

        public CarRepository(CarDBContext ctx) : base(ctx)
        {
        }

        public ObservableCollection<Car> GetObservable(List<Car> cars)
        {
            IEnumerable<Car> list = cars;
           
            var oc = new ObservableCollection<Car>();
            foreach (var item in list)
                oc.Add(item);

            return oc;
        }

        public Car GetOne(int id)
        {
            using (var ctx = new CarDBContext())
            {
                var result = ctx.Cars.Where(x => x.CarId == id).Single();
                return result;
            }
        }
        public ObservableCollection<Car> GetCarsByMake(string make)
        {
            using (var ctx = new CarDBContext())
            {
                var result = ctx.Cars.Where(x => x.Make.Contains(make)).ToList();
                return GetObservable(result);
            }
        }
        public ObservableCollection<Car> GetCarsOrderByMake(string make)
        {
            using (var ctx = new CarDBContext())
            {
                var result = ctx.Cars.OrderBy(x => make).ToList();
                return GetObservable(result);
            }
        }
    }
}
