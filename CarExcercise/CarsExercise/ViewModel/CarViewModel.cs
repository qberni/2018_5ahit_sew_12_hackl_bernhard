﻿using CarsExercise.Command;
using CarsExercise.Context;
using CarsExercise.Model;
using CarsExercise.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise
{
    class CarViewModel : AResources
    {
        private CarRepository carRepository = new CarRepository();

        public IAsyncCommand CreateCar { get; set; }
        public IAsyncCommand DeleteCar { get; set; }
        public IAsyncCommand UpdateCar { get; set; }

        private String _color;
        private String _make;
        private String _carnickname;
        private String _searchCar;
        private Car _selectedCar;

        public String CarNickName { get { return _carnickname; } set { _carnickname = value; Notify(); } }
        public String Color { get { return _color; } set { _color = value; Notify(); } }
        public String Make { get { return _make; } set { _make = value; Notify(); } }
        public String SearchCar { get { return _searchCar; } set { _searchCar = value; Notify(); Cars = carRepository.GetCarsByMake(_searchCar); Notify("Cars"); } } 
        public Car SelectedCar
        {
            get { return _selectedCar; }
            set
            {
                if (value != null)
                {
                    _selectedCar = value;
                    _carnickname = _selectedCar.CarNickName;
                    _color = _selectedCar.Color;
                    _make = _selectedCar.Make;

                    Notify("CarNickName");
                    Notify("Color");
                    Notify("Make");
                    Notify();
                }
            }
        }


        public CarViewModel()
        {
            ChangeView();
            CreateCar = new AsyncCommand(
               async () =>
               {
                   carRepository.Create(new Car(Make, Color, CarNickName));

                   await ChangeView();
               });
            DeleteCar = new AsyncCommand(
                async () =>
                {
                    if (_selectedCar != null)
                    {
                        carRepository.Delete(_selectedCar);
                    }

                    await ChangeView();

                });
            UpdateCar = new AsyncCommand(
                async () =>
                {
                    _selectedCar.CarNickName = CarNickName;
                    _selectedCar.Make = Make;
                    _selectedCar.Color = Color;
                    carRepository.Update(SelectedCar);

                    await ChangeView();
                });
        }
     
    }
}
