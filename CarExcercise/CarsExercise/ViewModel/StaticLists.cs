﻿using CarsExercise.Context;
using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise
{
    abstract class AResources : ANotify
    {
        public ObservableCollection<Car> Cars
        {
            get;
            set;
        } = new ObservableCollection<Car>();

        public  ObservableCollection<Order> Orders
        {
            get;
            set;
        } = new ObservableCollection<Order>();
        public  ObservableCollection<CreditRisk> CreditRisks
        {
            get; set;

        } = new ObservableCollection<CreditRisk>();
        public  ObservableCollection<Customer> Customers
        {
            get;
            set;
        } = new ObservableCollection<Customer>();

        public async Task ChangeView()
        {
            using (var context = new CarDBContext())
            {
                Cars.Clear();
                Orders.Clear();
                Customers.Clear();
                CreditRisks.Clear();
                (await context.Cars.ToListAsync())
                    .ForEach(c => Cars.Add(c));
                (await context.Orders.ToListAsync())
                    .ForEach(o => Orders.Add(o));
                (await context.Customers.ToListAsync())
                     .ForEach(c => Customers.Add(c));
                (await context.CreditRisks.ToListAsync()).ForEach(c => CreditRisks.Add(c));


            }

        }
    }
}
