﻿using CarsExercise.Command;
using CarsExercise.Context;
using CarsExercise.Model;
using CarsExercise.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise
{
    class CreditRiskViewModel : AResources
    {
        CreditRiskRepository creditRiskRepository = new CreditRiskRepository();

        public IAsyncCommand CreateCreditRisk { get; set; }
        public IAsyncCommand DeleteCreditRisk { get; set; }

        private Customer _selectedCreditRiskCustomers;

        public Customer SelectedCreditRiskCustomer
        {
            get { return _selectedCreditRiskCustomers; }
            set
            {

                _selectedCreditRiskCustomers = value;

                Notify();

            }
        }
        public CreditRisk _selectedCreditRisk;
        public CreditRisk SelectedCreditRisk
        {
            get { return _selectedCreditRisk; }
            set
            {
                if (value != null)
                {

                    _selectedCreditRisk = value;
                    Notify();


                }
            }
        }

        public CreditRiskViewModel()
        {
            ChangeView();

            CreateCreditRisk = new AsyncCommand(
            async () =>
            {
                creditRiskRepository.Create(new CreditRisk(SelectedCreditRiskCustomer.FirstName, SelectedCreditRiskCustomer.LastName, SelectedCreditRiskCustomer.CustId));

                await ChangeView();
            }
            );
            DeleteCreditRisk = new AsyncCommand(
                async () =>
                {
                    creditRiskRepository.Delete(SelectedCreditRisk);

                    await ChangeView();
                }
                );
        }
    }
}
