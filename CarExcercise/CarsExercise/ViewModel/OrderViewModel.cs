﻿using CarsExercise.Command;
using CarsExercise.Context;
using CarsExercise.Model;
using CarsExercise.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CarsExercise
{
    class OrderViewModel : AResources
    {
       
        ARepository<Order> orderRepository = new OrderRepository();


        public IAsyncCommand CreateOrder { get; set; }
        public IAsyncCommand DeleteOrder { get; set; }
        public IAsyncCommand UpdateOrder { get; set; }

      
        private Car _selectedCarOrder;
        private Customer _selectedCustomerOrder;

       


        private Order _selectedOrder;
        public Order SelectedOrder
        {
            get
            {
                return _selectedOrder;
            }

            set
            {
                if (value != null)
                {
                    _selectedOrder = value;
                    _selectedCarOrder = _selectedOrder.Car;
                    _selectedCustomerOrder = _selectedOrder.Customer;
                    Notify("SelectedCarOrder");
                    Notify("SelectedCustomerOrder");
                    Notify();
                }
            }
        }

        public OrderViewModel()
        {
            
            ChangeView();
           
           
           
            CreateOrder = new AsyncCommand(async () => {
                orderRepository.Create(new Order(SelectedCustomerOrder, SelectedCarOrder));
          
                await ChangeView();
            });
            DeleteOrder = new AsyncCommand(
               async () => {
                   if (_selectedOrder != null)
                   {
                       orderRepository.Delete(_selectedOrder);
                   }
       
                   await ChangeView();
               });
            UpdateOrder = new AsyncCommand(
               async () =>
               {
                   _selectedOrder.Customer = SelectedCustomerOrder;
                   _selectedOrder.Car = SelectedCarOrder;
                   _selectedOrder.CarId = SelectedCarOrder.CarId;
                   _selectedOrder.CustId = SelectedCustomerOrder.CustId;
                   orderRepository.Update(SelectedOrder);

                   await ChangeView();
               });
        
        }

       
     


        public Car SelectedCarOrder
        {
            get
            {
                return _selectedCarOrder;
            }

            set
            {
                _selectedCarOrder = value;
                Notify();
            }
        }

        public Customer SelectedCustomerOrder
        {
            get
            {
                return _selectedCustomerOrder;
            }

            set
            {
                _selectedCustomerOrder = value;
                Notify();
            }
        }

        


    }
}
