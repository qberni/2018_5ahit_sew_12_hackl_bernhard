﻿using CarsExercise.Command;
using CarsExercise.Context;
using CarsExercise.Model;
using CarsExercise.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise
{
    class CustomerViewModel : AResources
    {
        CustomerRepository customerRepository = new CustomerRepository();

        public IAsyncCommand CreateCustomer { get; set; }
        public IAsyncCommand DeleteCustomer { get; set; }
        public IAsyncCommand UpdateCustomer { get; set; }

        private String _firstname;
        private String _lastname;

        private Customer _selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                if (value != null)
                {
                    _selectedCustomer = value;
                    _firstname = _selectedCustomer.FirstName;
                    _lastname = _selectedCustomer.LastName;

                    Notify("LastName");
                    Notify("FirstName");
                    Notify();
                }
            }
        }
        public string FirstName
        {
            get
            {
                return _firstname;
            }

            set
            {
                _firstname = value;
                Notify();
            }
        }

        public string LastName
        {
            get
            {
                return _lastname;
            }

            set
            {
                _lastname = value;
                Notify();
            }
        }
    
        public CustomerViewModel()
        {
            ChangeView();
            CreateCustomer = new AsyncCommand(
               async () => {
                   customerRepository.Create(new Customer(FirstName, LastName));

                   await ChangeView();
               });
            DeleteCustomer = new AsyncCommand(
                async () => {
                    if (_selectedCustomer != null)
                    {
                        customerRepository.Delete(_selectedCustomer);
                    }

                    await ChangeView();
                });
            UpdateCustomer = new AsyncCommand(
                async () =>
                {
                    _selectedCustomer.FirstName = FirstName;
                    _selectedCustomer.LastName = LastName;
                    customerRepository.Update(SelectedCustomer);

                    await ChangeView();
                });
        }
    }
}
