﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Model
{
    class Order
    {
        public Order() { }
        public Order(Customer selectedCustomerOrder, Car selectedCarOrder)
        {
            this.Customer = selectedCustomerOrder;
            this.Car = selectedCarOrder;
            CustId = selectedCustomerOrder.CustId;
            CarId = selectedCarOrder.CarId;
            
        }

        [Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }

        [Required]
        public int CustId { get; set; }
        [Required]
        public int CarId { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }

        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("CarId")]
        public virtual Car Car { get; set; }

        public override string ToString()
        {
            return OrderId + " " + Car.Make + " " + Customer.FullName;
        }
    }
}
