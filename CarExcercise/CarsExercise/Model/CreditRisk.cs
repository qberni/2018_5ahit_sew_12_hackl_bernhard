﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Model
{
    partial class CreditRisk
    {
        public CreditRisk() { }
        public CreditRisk(string firstName, string lastName, int CustId)
        {
            FirstName = firstName;
            LastName = lastName;
            this.CustId = CustId;
        }

        [Key]
        public int CustId { get; set; }

        [StringLength(50)]
        [Index("IDX_CreditRisk_Name", IsUnique = true, Order = 2)]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Index("IDX_CreditRisk_Name", IsUnique = true, Order = 1)]
        public string LastName { get; set; }

        [Timestamp]
        public byte[] TimeStamp { get; set; }
    }
}
