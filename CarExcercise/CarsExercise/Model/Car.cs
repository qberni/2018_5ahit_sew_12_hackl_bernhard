﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Model
{
    [Table("Car")]
    partial class Car
    {
        public Car() { }
        public Car(string make, string color, string carnickname)
        {
            Make = make;
            Color = color;
            CarNickName = carnickname;
        }
        [Key]
        public int CarId { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50)]
        public string CarNickName { get; set; }

        public string MakeColor => Color + " " + Make;
        public virtual ICollection<Order> orders { get; set; } = new HashSet<Order>();

        [Timestamp]
        public byte[] Timestamp { get; set; }


    }
}
