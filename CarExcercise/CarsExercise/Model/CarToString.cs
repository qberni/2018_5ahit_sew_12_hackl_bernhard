﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Model
{
    partial class Car
    {
        public override string ToString()
        {
            return $"{Make} " + $"{CarNickName}" + $" --- Farbe: {Color}" ?? "no Color";
        }
    }
}
