﻿using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace CarsExercise
{
    public static class CustomExtensions
    {
        public static string UpperFirstChar(this string value)
        {
            if (value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return value;
        }

        public static int MultiplyBy(this int value, int multiplier)
        {
            return value * multiplier;
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> list) where T : class
        {
            var oc = new ObservableCollection<T>();
            foreach (var item in list)
                oc.Add(item);

            return oc;
        }

    }
}