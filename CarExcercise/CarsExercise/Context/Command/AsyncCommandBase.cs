﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CarsExercise.Command
{
    abstract class AsyncCommandBase : IAsyncCommand
    {
        public abstract bool CanExecute(object param);
        public abstract Task ExecuteAsync(object param);
        public async void Execute(object parameter)
        {
            
                await ExecuteAsync(parameter);
        }
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        protected void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }
    }
}
