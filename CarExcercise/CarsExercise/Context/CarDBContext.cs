﻿using CarsExercise.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsExercise.Context
{
    class CarDBContext : DbContext
    {
        public CarDBContext()
        {
            Database.SetInitializer<CarDBContext>(new CarDBInitializer());
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<CreditRisk> CreditRisks { get; set; }
    }
}
