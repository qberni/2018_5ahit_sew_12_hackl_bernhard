﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolExcersice
{
    [Table("Book")]
    class Book
    {
        [Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int BookID { get; set; }

        [StringLength(50)]
        [Required]
        public string Title { get; set; }

        [Required]
        public int Pages { get; set; }

        [Required]
        public float Price { get; set; }


    }
}
