﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolExcersice
{
    [Table("School")]
    class School
    {
        [Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SchoolID { get; set; }

        public string Name { get; set; }
    }
}
