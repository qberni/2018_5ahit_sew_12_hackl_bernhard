﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolExcersice.Model
{
    [Table("Subject")]
    class Subject 
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}
