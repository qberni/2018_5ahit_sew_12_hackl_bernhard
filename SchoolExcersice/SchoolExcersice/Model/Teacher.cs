﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolExcersice.Model
{
    [Table("Teacher")]
    class Teacher : APerson
    {
     
        public Order OrderId { get; set; }

  
        public School School { get; set; }

        public Subject Subjects { get; set; }
    }
}
