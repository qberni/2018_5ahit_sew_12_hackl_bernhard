﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolExcersice
{
    [Table("Order")]
    class Order
    {
        [Key, Required, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        int OrderID { get; set; }

        [ForeignKey("StudentID")]
        public virtual Student StudentID { get; set; }

        [ForeignKey("BookID")]
        public virtual Book BookID { get; set; }

        
    }
}
